#!/usr/bin/env python3

import socket
import queue
import threading
import dome

USER_PORT    = 23502
DOME_ADDRESS = 'localhost'
DOME_PORT    = 5000


def userToDomeCommand(command):
    # no need of parsing (for now)
    return command


def domeToUserStatus(status):
    # no need of parsing
    return status


def domeServer(cmd_queue):
    dome_interface = dome.Interface()
    while True:
        try:
            msg = queue.get(0)
        except cmd_queue.Empty:
            pass


def userServer(cmd_queue):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('0.0.0.0', USER_PORT))
    s.listen(2)
    print('Listening on port', str(USER_PORT))
    while True:
        conn, addr = s.accept()
        print('Connected by', addr)
        data = conn.recv(1024)
        command = userToDomeCommand(data)
        cmd_queue.put(command)
        conn.sendall(bytes(0x42))
        conn.close()


command_queue = queue.Queue()

dome_server = threading.Thread(target=domeServer, args=(command_queue,))
user_server = threading.Thread(target=userServer, args=(command_queue,))

dome_server.start()
user_server.start()
