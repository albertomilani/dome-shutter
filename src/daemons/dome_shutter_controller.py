#!/usr/bin/env python3

import socket
import queue
import threading
import logging
import struct

PLC_PORT = 30000
USER_PORT = 23501

logging.basicConfig(level=logging.DEBUG)

def userToPlcCommand(in_command):
    global plc_power_on
    in_command = in_command.strip()
    command = 0
    if in_command == 'OPEN_SUP':
        command = 1 << 0
    elif in_command == 'OPEN_INF':
        command = 1 << 1
    elif in_command == 'CLOSE_SUP':
        command = 1 << 2
    elif in_command == 'CLOSE_INF':
        command = 1 << 3
    elif in_command == 'POWER_ON':
        plc_power_on = 1
    elif in_command == 'POWER_OFF':
        plc_power_on = 0

    # set power on bit
    if plc_power_on == 1:
        command = command | (1 << 4)

    # set 6th bit always at 1 
    command = command | (1 << 5)

    if command:
        command = struct.pack('c', bytes([command]))

    return command


def plcToUserStatus(status):
    # no need of parsing
    return status


def plcServer(cmd_queue):
    global plc_status
    global plc_power_on
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(('0.0.0.0', PLC_PORT))
    s.listen(2)
    logging.debug('Listening on port ' + str(PLC_PORT))
    while True:
        conn, address = s.accept()
        try:
            logging.debug('Connected by ' + str(address))
            # receiving status
            data = conn.recv(1024)
            logging.debug('Received: ')
            logging.debug(data)
            #print(data)
            if data:
                plc_status = plcToUserStatus(data)
            # check if there is any queue element
            if not cmd_queue.empty():
                # get first queue element
                msg = cmd_queue.get(0)
                # convert message to byte
                conn.sendall(msg)
            # otherwise send a 00100000 byte
            else:
                out_byte = 32
                if plc_power_on == 1:
                    out_byte = out_byte | (1 << 4)
                conn.sendall(bytes([out_byte]))
                logging.info('Send default byte ' + str(out_byte))
            logging.debug('Closing connection')
        except Exception as e:
            logging.error('ERROR: ' + str(e))
        conn.close()


def userServer(cmd_queue):
    global plc_status
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(('0.0.0.0', USER_PORT))
    s.listen(2)
    logging.debug('Listening on port ' + str(USER_PORT))
    while True:
        conn, address = s.accept()
        try:
            logging.debug('Connected by ' + str(address))
            # receiving data
            data = conn.recv(1024)
            raw_command = data.decode("utf-8")
            logging.debug('Command received: ' + str(raw_command))
            if raw_command and raw_command != 'GET_STATUS':
                # parsing data
                command = userToPlcCommand(raw_command)
                logging.debug('Parsed command: ' + str(command))
                # sending command
                cmd_queue.put(command)
            # always send plc status
            logging.debug('PLC status: ' + str(plc_status))
            conn.sendall(plc_status)
        except Exception as e:
            logging.error('ERROR: ' + str(e))
        conn.close()


command_queue = queue.Queue()
plc_status = bytes(10)

plc_power_on = 0

plc_server = threading.Thread(target=plcServer, args=(command_queue,))
user_server = threading.Thread(target=userServer, args=(command_queue,))

plc_server.start()
user_server.start()
