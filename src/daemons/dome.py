import serial
import socket

class Interface:

    TYPE_USB = "USB"
    TYPE_ETH = "ETH"

    def __init__(self):
        self.arduino = None
        self.type = None

    #####################
    # Internal commands #
    #####################

    def connect(self, device = None, speed = None, address = None, port = None):
        if device is not None and speed is not None:
            self.arduino = serial.Serial(device, speed)
            self.type = TYPE_USB
        elif address is not None and port is not None:
            self.arduino = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.arduino.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)
            self.arduino.connect((address, port))
            self.type = TYPE_ETH
        else:
            raise Exception("Invalid connection parameters")

    def sendCommand(self, command):
        if self.arduino is not None:
            self.arduino.write(command.encode())

    def read(self):
        string = None
        if self.arduino is not None:
            string = self.arduino.readline().decode('utf-8')
        return string

    #####################
    # External commands #
    #####################

    def rotate(self, direction, degree):
        return True

    def getPosition(self):
        return True

