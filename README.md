# Dome controller

## How to install
Clone the repo into /opt
Put: `utils/dome-shutter.service` in `/lib/systemd/system/dome-shutter.service`

To start daemon:
```
systemctl start dome-shutter
```

To start daemon at boot:
```
systemctl enable dome-shutter
```

## Description

### Shutter
See `docs/VARIABILI_SOCKET_PLC.pdf`.

### Rotation
TODO :exclamation:
