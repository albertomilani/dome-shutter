#!/usr/bin/env python3

import socket
import struct

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('localhost', 30000))
data = struct.pack('I', int('11111111', 2))
print(data)
s.sendall(data)
data = s.recv(5)
if data:
    print(data)
