#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from timeloop import Timeloop
from datetime import timedelta
import tkinter
import socket
import sys
from tkinter import *
#Tk, BOTH, NORMAL,from threading import Thread

import time
import math
import bitstring
from bitstring import *
import threading
import time
t_lock = threading.Lock()
# tl = Timeloop()
# data=b'\xFF\xFF'
root = tkinter.Tk()
root.title('Server')
btn_stop_clicked=0
btn_Poweron_stat=0
start=0
end=0
speed_time=StringVar()
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
premuto=False
stato_plc=0
pos_inf=0
pos_sup=0
aux_Poweron_stat=True


# variabili comandi
comandi=BitArray(uint=0, length=8)
comandi.set(0) #resetta comandi
# fine variabili comandi
# Bind the socket to the port
server_address = ('192.168.40.212', 30000)


class PLC(Frame):
	def __init__(self,master,names,state=True,**kw):
		self.name=StringVar()
		# if (self.name.get() == "None"):
		# 	self.name = "PLC_VAR"
		# else:
		self.name.set(names)

		Frame.__init__(self,master,width=150,height=20,relief=SUNKEN,bd=1,padx=5,pady=5)
		self.configure(**kw)
		self.state = True
		self.Label = Label(self,textvariable=self.name)
		self.led = LED(self,20)
		self.Label.grid(column=0,row=0)
		self.led.grid(column=3,row=0)

	# def updateLED(self):
	#      self.led.set(state)

	def set(self,state):
		self.led.set(state)
		self.state = state
		# self.updateLED()

	def set_name(self,name):
		self.name.set(name)
		self.name=name

	def get_name(self):
		return self.name.get()

	def get_state(self):
		return self.state








class LED(Frame):
	"""A Tkinter LED Widget.
	a = LED(root,10)
	a.set(True)
	current_state = a.get()"""
	OFF_STATE = 0
	ON_STATE = 1

	def __init__(self,master,size=10,**kw):
		self.size = size
		Frame.__init__(self,master,width=size,height=size)
		self.configure(**kw)
		self.state = LED.ON_STATE
		self.c = Canvas(self,width=self['width'],height=self['height'])
		self.c.grid()
		self.led = self._drawcircle((self.size/2)+1,(self.size/2)+1,(self.size-1)/2)
	def _drawcircle(self,x,y,rad):
		# """Draws the circle initially"""
		color="red"
		return self.c.create_oval(x-rad,y-rad,x+rad,y+rad,width=rad/5,fill=color,outline='black')
	def _change_color(self):
		"""Updates the LED colour"""
		if self.state == LED.ON_STATE:
			color="green"
		else:
			color="red"
		self.c.itemconfig(self.led, fill=color)
	def set(self,state):
		"""Set the state of the LED to be True or False"""
		self.state = state
		self._change_color()
	def get(self):
		"""Returns the current state of the LED"""
		return self.state

	def readStates(self):
		"""Cycles through the assigned ports and updates them based on the GPIO input"""
		for port in self.ports:
			port.updateInput()

	def update(self):

		self.readStates()
		self._timer = self.after(100,self.update)

def sk_timer(wait_time): #miliiseconds
	set_time_ms=wait_time
	while True:
		start = time.clock()
		if  (1000*start)%set_time_ms==0.0:
			end=time.clock()
			#print ("Time elapsed = ", set_time_ms+((end-start)*1000), "mseconds")
			break

def btn_quit_clicked(event):
	btn_quit.focus_set()
	print ("clicked at", event.x, event.y, event.type)
	t_skt.join()
		#if tkMessageBox.askokcancel("Quit", "Do you really wish to quit?"):
		#root.destroy()
	#import sys; sys.exit()
	global premuto
	print(premuto)
	premuto=True
	print('closing socket')
	sock.close()
	tl.stop()
	root.destroy()



def bblink(plc_led):
	v=plc_led.get()
	print(v,"got")
	if plc_led.get() == True:
		plc_led.set(False)
	else:
		plc_led.set(True)
	root.update()




def apri_vano_sup(event):
	global comandi
	btn_sk_start.configure(state=NORMAL)
	btn_csup.configure(state=DISABLED)
	comandi.set(True,-1)
	comandi.set(False,-3)
	print("apro vano superiore ",comandi.uint)
	time.sleep(0.3)

def apri_vano_sup_rel(event):
	global comandi
	time.sleep(0.5)
	comandi.set(False,-1)

def apri_vano_inf(event):
	global comandi
	btn_cinf.configure(state=DISABLED)
	comandi.set(True,-2)
	comandi.set(False,-4)
	print("apro vano inferiore ",comandi.uint)
	time.sleep(0.3)
	
def apri_vano_inf_rel(event):
	global comandi
	time.sleep(0.5)
	comandi.set(False,-2)

def chiudi_vano_sup(event):
	global comandi
	btn_asup.configure(state=DISABLED)
	comandi.set(True,-3)
	comandi.set(False,-1)
	print("chiudo vano superiore ",comandi.uint)
	time.sleep(0.3)
	
def chiudi_vano_sup_rel(event):
	global comandi
	time.sleep(0.5)
	comandi.set(False,-3)

def chiudi_vano_inf(event):
	global comandi
	btn_ainf.configure(state=DISABLED)
	comandi.set(True,-4)
	comandi.set(False,-2)
	print("chiudo vano inferiore ",comandi.uint)
	btn_cinf.configure(state=DISABLED)
	time.sleep(0.3)

def chiudi_vano_inf_rel(event):
	global comandi
	time.sleep(0.5)
	comandi.set(False,-4)


def PC_Power():
	global comandi
	global aux_Poweron_stat
	global btn_Poweron_stat
	global stato_plc
	print("ARMAREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE")
	print ("STATO",stato_plc)
	print("aux_power_on:",aux_Poweron_stat)
	st_pwr=aux_Poweron_stat
	st_plc=stato_plc
	
	if st_plc=="105":
		if st_pwr==True:
			comandi.set(False,-5)#OFF
			btn_PC_Power.configure(text='PC_POWER_ON / RESET',bg='green2')
			# comandi.set(False,-1)
			# comandi.set(False,-2)
			# comandi.set(False,-3)
			# comandi.set(False,-4)
			# comandi.set(False,-6)
			# btn_csup.configure(state=NORMAL)
			# btn_cinf.configure(state=NORMAL)
			# btn_ainf.configure(state=NORMAL)
			# time.sleep(0.5)
			
		else:
			btn_PC_Power.configure(text='PC_POWER_OFF / RESET',bg='red')
			comandi.set(True,-5)
			print("power_on ",comandi.uint)
			# comandi.set(False,-1)
			# comandi.set(False,-2)
			# comandi.set(False,-3)
			# comandi.set(False,-4)
			# comandi.set(False,-6)
			# btn_csup.configure(state=NORMAL)
			# btn_cinf.configure(state=NORMAL)
			# btn_ainf.configure(state=NORMAL)
			# time.sleep(0.5)
	else:
		comandi.set(False,-5)
		btn_PC_Power.configure(text='PC_POWER_ON / RESET',bg='green2')
		# comandi.set(False,-1)
		# comandi.set(False,-2)
		# comandi.set(False,-3)
		# comandi.set(False,-4)
		# comandi.set(False,-6)
		# btn_csup.configure(state=NORMAL)
		# btn_cinf.configure(state=NORMAL)
		# btn_asup.configure(state=NORMAL)
		# btn_ainf.configure(state=NORMAL)
	clear_commands()
	time.sleep(0.5)

def clear_commands():
	comandi.set(False,-1)
	comandi.set(False,-2)
	comandi.set(False,-3)
	comandi.set(False,-4)
	comandi.set(False,-6)
	btn_csup.configure(state=NORMAL)
	btn_cinf.configure(state=NORMAL)
	btn_asup.configure(state=NORMAL)
	btn_ainf.configure(state=NORMAL)

# def PC_Power_clk(event):
	# global comandi
	# clear_commads()
	# comandi.set(True,-5)
	# print("power_off ",comandi.uint)
	# btn_Poweron_stat=1
	# btn_PC_Power.configure(text="POWER OFF", bg='red')
	# time.sleep(0.3)

# def PC_Power_rel(event):
	# global comandi
	# time.sleep(0.3)
	# comandi.set(False,-5)
	# print("power_on ",comandi.uint)
	# #btn_PC_Reset.configure(state=DISABLED)
	# btn_Poweron_stat=0
	# btn_PC_Power.configure(text="POWER ON", bg='green2')

def vani_stop_clk(event):
	global comandi
	# btn_csup.configure(state=DISABLED)
	# btn_cinf.configure(state=DISABLED)
	# btn_asup.configure(state=DISABLED)
	# btn_cinf.configure(state=DISABLED)
	btn_csup.configure(state=NORMAL)
	btn_cinf.configure(state=NORMAL)
	btn_asup.configure(state=NORMAL)
	btn_ainf.configure(state=NORMAL)
	comandi.set(False,-1)
	comandi.set(False,-2)
	comandi.set(False,-3)
	comandi.set(False,-4)
	comandi.set(True,-6) # comando stop
	time.sleep(0.3)


def vani_stop_rel(event):
	global comandi
	comandi.set(False,-6)
	time.sleep(0.3)


def l_speed_upd(val_speed_time):
	var="% 12.2f" % val_speed_time
	var_s=str(var)
	speed_time.set(str(var_s))


def skstart():
	global btn_stop_clicked
	global t_skt
	# global t_update
	btn_sk_start.configure(state=DISABLED)
	btn3.configure(bg='red2')
	btn_sk_stop.configure(state=NORMAL)
	btn_sk_stop.focus_set()
	btn_stop_clicked=0
	#btn_sk_stop.focus_set()

	# t_skt = threading.Thread(target = skt_server)
	t_skt.start()
	# t_update.start()
	# print("update is alive: ",t_update.isAlive())
	print("socket is alive: ",t_skt.isAlive())
	# root.update()
	# time.sleep(1)
def skstop():
	global btn_stop_clicked
	global t_skt
	btn_sk_start.configure(state=NORMAL)
	btn_sk_stop.configure(state=DISABLED)
	btn_sk_start.focus_set()
	btn_stop_clicked=1
	print('closing socket button ')
	# sock.close()
	t_skt.join()
	#t_update.join()
	# tl.stop()
	time.sleep(0.5)
	print(btn_stop_clicked)

def skt_server():
	run_task=0
	
	# data=b'\xFF\xFF'
	print("debug 1")
	counter=0
	global btn_stop_clicked
	global comandi
	# global data
	#server side start
	# Create a TCP/IP socket
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	# t_update=threading.Timer(1.0, update_plc_vars,(data, ))
	# t_update.start()
	# Bind the socket to the portbtn_stop
	server_address = ('192.168.40.212', 30000)
	print('starting up on {} port {}'.format(*server_address))
	try:
		sock.bind(server_address)
	except:
		return


	# Listen for incoming connections
	sock.listen(1)
	#print(str(btn_sk_stop['state']))
	print("debug 2")

	#while True:
	while btn_stop_clicked == 0:
		print("debug 3")
		start=time.clock()
		if(btn_stop_clicked==1):
			sock.close()
			break
			return
		#root.update()
		# Wait for a connection
		print(str(btn_sk_stop['state']))
		print('waiting for a connection')
		connection, client_address = sock.accept()
		#root.update()
		try:
			print("debug 4")
			print('connection from', client_address)
			# end=time.clock()
			# val_speed_time=(end-start)*1000
			# print ('speed= ', (end-start)*1000, 'ms')
			# l_speed_upd(val_speed_time)
			# Receive the data in small chunks and retransmit i
			while btn_stop_clicked == 0:
				#root.update()
				if(btn_stop_clicked==1):
					sock.close()
					break
					return
				# data = connection.recv(4)
				data = connection.recv(12)
				#update_plc_vars(data)
				print('received {!r}'.format(data))
				print("debug 5")
				if data:
					print('sending data back to the client')
					connection.sendall(comandi.tobytes())
					update_plc_vars(data)
					print("UPDATE")
					print("comand: ",comandi.uint, "   " , comandi.tobytes())
					end=time.clock()
					print ("TIMER",(end-start)*1000)
					
					# if run_task==0:
						# t_update=threading.Timer(0.3, update_plc_vars,(data, ))
						# run_task=0
						# t_update.start()
						# t_update.join()
						
					
				else:
					('no data from', client_address)
					print("debug 6")
					# root.update()
					start=time.clock()
					#t_update.join()
					break

			counter=counter+1
			print(counter)
			print()
			end=time.clock()
			val_speed_time=(end-start)*1000
			print ('speed= ', val_speed_time, 'ms')
			l_speed_upd(val_speed_time)
			print("debug 7")
			# return data
		except:
			connection.close()
			sock.close()
			t_update.join()
			break
			return
		finally:
			# Clean up the connection
			connection.close()
			# root.update()
			print("debug 8")
			sk_timer(100)
	return


try:
	t_skt = threading.Thread(target = skt_server)
except:
	print("closing t_skt")
	t_skt.join()



def btn3_click():
	btn_quit.focus_set()
	global premuto
	print("pippo")
	for i in range(0,13):
		if premuto==True:
			break
		bblink(plc2)
		sk_timer(300)
		bblink(plc1)
		#bisogna rinfrescare l'interfaccia
		#altrimenti non si vede il led lampeggiare



#e' solo un promemoria per il metodo di calcolo
def bytes_to_int(bytes):
	result = 0
	for b in bytes:
		 # result = result * 256 + int(b)
		 result = result * 256 + int(b)
	return result

root.columnconfigure(0, pad=5)
root.columnconfigure(1, pad=5)
root.columnconfigure(2, pad=5)
root.columnconfigure(3, pad=5)
root.columnconfigure(4, pad=5)
num_rows=15
for i in range(0,num_rows):
	root.rowconfigure(i, pad=5)


btn_sk_start = tkinter.Button(root, text='Start Socks', state=NORMAL, bg='green2', command=skstart)
btn_sk_start.grid(column=0, row=0)

btn_sk_stop = tkinter.Button(root, text='Stop Socks', state=DISABLED, bg='red2', command=skstop)
btn_sk_stop.grid(column=1, row=0)

btn3 = tkinter.Button(root, text='Button 3',command=btn3_click)
btn3.grid(column=2, row=0)



l_speed = tkinter.Label(textvariable=speed_time, text="Speed", fg="black", bg="white")
l_speed.grid(column=0, row=3)
l2_output = tkinter.Label(text="speed (ms)", fg="black", bg="white")
l2_output.grid(column=1, row=3)

#----------------my_function-----------------COMANDI---------------------------------

btn_asup = tkinter.Button(root, text='Apri Sup.', state=NORMAL, bg='green2')
btn_asup.bind("<ButtonPress>", apri_vano_sup)
btn_asup.bind("<ButtonRelease>", apri_vano_sup_rel)
btn_asup.grid(column=2, row=6)

btn_ainf = tkinter.Button(root, text='Apri Inf.', state=NORMAL, bg='green2')
btn_ainf.bind("<ButtonPress>", apri_vano_inf)
btn_ainf.bind("<ButtonRelease>", apri_vano_inf_rel)
btn_ainf.grid(column=3, row=6)

btn_csup = tkinter.Button(root, text='Chiudi Sup.', state=NORMAL, bg='green2')
btn_csup.bind("<ButtonPress>", chiudi_vano_sup)
btn_csup.bind("<ButtonRelease>", chiudi_vano_sup_rel)
btn_csup.grid(column=2, row=5)

btn_cinf = tkinter.Button(root, text='Chiudi Inf.', state=NORMAL, bg='green2')
btn_cinf.bind("<ButtonPress>", chiudi_vano_inf)
btn_cinf.bind("<ButtonRelease>", chiudi_vano_inf_rel)
btn_cinf.grid(column=3, row=5)

# btn_vani_stop = tkinter.Button(root, text='STOP', state=NORMAL, bg='green2', command=vani_stop)
btn_vani_stop = tkinter.Button(root, text='STOP', state=NORMAL, bg='green2')
btn_vani_stop.bind("<ButtonPress>", vani_stop_clk)
btn_vani_stop.bind("<ButtonRelease>", vani_stop_rel)
btn_vani_stop.grid(column=4, row=5)

# self.button.bind("<ButtonPress>", self.on_press)
# self.button.bind("<ButtonRelease>", self.on_release)

btn_PC_Power = tkinter.Button(root, text='PC_POWER_ON / RESET', state=NORMAL, bg='green2',command=PC_Power)
# btn_PC_Power = tkinter.Button(root, text='PC_POWER_ON / RESET', state=NORMAL, bg='red')
# if aux_Poweron_stat==True:
	# btn_PC_Power.text='PC_POWER_OFF / RESET'
	# btn_PC_Power.bg='red'
# else:
	# btn_PC_Power.text='PC_POWER_ON / RESET'
	# btn_PC_Power.bg='green2'
	
btn_PC_Power.grid(column=4, row=6)
# btn_PC_Power.bind("<ButtonPress>", PC_Power_clk)
# btn_PC_Power.bind("<ButtonRelease>", PC_Power_rel)

#------------------------------------FINE COMANDI-------------------------------
l2_stato_plc = tkinter.Label(text="STATO PLC:", fg="black", bg="white")
l2_stato_plc.grid(column=2, row=13)
l_stato_plc = tkinter.Label(text=stato_plc, fg="black", bg="white")
l_stato_plc.grid(column=3, row=13)
l3_pos_inf = tkinter.Label(text="POS. INFERIORE:", fg="black", bg="white")
l3_pos_inf.grid(column=2, row=14)
l3a_pos_inf = tkinter.Label(text=pos_inf, fg="black", bg="white")
l3a_pos_inf.grid(column=3, row=14)
l4_pos_sup = tkinter.Label(text="POS. SUPERIORE:", fg="black", bg="white")
l4_pos_sup.grid(column=2, row=15)
l4a_pos_sup = tkinter.Label(text=pos_sup, fg="black", bg="white")
l4a_pos_sup.grid(column=3, row=15)
#--------------------------------------INPUT/OUTPUT-----------------------------------
# plc1=PLC(root)
# plc1.set(False)
# plc1.set_name("pippo")
# plc1.grid(column=0, row=10)
# plc2=PLC(root)
# plc2.set(False)
# plc2.set_name("Topo")
# plc2.grid(column=0, row=11)
plc_var=[] #lista di oggetti PLC basati su vars
Plc_vars= [
	['Apre_sup',False,6,0],
	['Apre_inf',False,7,0],
	['Chiude_sup',False,8,0],
	['Chiude_inf',False,9,0],
	['Emergenza_OK',False,10,0],
	['Inverter1_OK',False,11,0],
	['Inverter2_OK',False,12,0],
	['Emergenza_V_sup',False,13,0],
	['V_Sup_Aperto',False,6,1],
	['V_Inf_Aperto',False,7,1],
	['LIBERO',False,8,1],
	['LIBERO',False,9,1],
	['Automatico',False,10,1],
	['AUX_POWER_ON',False,11,1],
	['Richiesto RIARMO',False,11,2],
	['MOTOR OVERLOAD',False,13,1]]

for k in range(len(Plc_vars)):
	print(k)
	plc_var.append(PLC(root,Plc_vars[k][0]))
	plc_var[k].set(Plc_vars[k][1])
	plc_var[k].grid(row=Plc_vars[k][2], column=Plc_vars[k][3])
# message = b'\xFF\xFF'  #b'\x40\x10'    #b'\xF1\x10'

# update_plc_vars()
# @tl.job(interval=timedelta(seconds=1))
def update_plc_vars(data):
	# bisogna verificare che il byte sia interpretato nell'ordine corretto -> si
	# global data
	global plc_var
	global stato_plc
	global pos_inf
	global pos_sup
	global aux_Poweron_stat
	message=data
	print("messaggio: ",message)
	b1=message[0]
	b2=message[1]
	#b3=message[2:5]
	input1=BitArray(uint=b1, length=8)
	input2=BitArray(uint=b2, length=8)

	# print("message[2:5]", message[2:4])
	# print("BYTE1: ",b1,input1)
	# print("BYTE2: ",b2,input2)
	# print("BYTE3: ",b3)#,input3)
	# print("b3:=",int.from_bytes(b3, "little"))
	# print("INPUT1: ",input1[-8],input1[-7],input1[-6],input1[-5],input1[-4],input1[-3],input1[-2],input1[-1])

	pos_inf=str(int.from_bytes(message[4:7], "little"))
	pos_sup=str(int.from_bytes(message[7:10], "little"))
	stato_plc=str(int.from_bytes(message[2:4], "little"))
	# stato_plc=bytes_to_int(message[2:5])#(b'\x27\x0f')
	print(stato_plc)

	print("INPUT2: ",input2[-8],input2[-7],input2[-6],input2[-5],input1[-4],input2[-3],input2[-2],input2[-1])
	for k in range(1,9):
		plc_var[k-1].set(input1[-k]) #bit 1 dx, bit less sign. a dx
		plc_var[(k-1)+8].set(input2[-k])




	aux_Poweron_stat=plc_var[13].get_state()
	# print("AUX_POWER_ONNNNNNNNNNN",aux_Poweron_stat)

	l2_stato_plc = tkinter.Label(text="STATO PLC:", fg="black", bg="white")
	l2_stato_plc.grid(column=2, row=13)
	l_stato_plc = tkinter.Label(text=stato_plc, fg="black", bg="white")
	l_stato_plc.grid(column=3, row=13)


	l3_pos_inf = tkinter.Label(text="POS. INFERIORE:", fg="black", bg="white")
	l3_pos_inf.grid(column=2, row=14)
	l3a_pos_inf = tkinter.Label(text=pos_inf, fg="black", bg="white")
	l3a_pos_inf.grid(column=3, row=14)

	l4_pos_sup = tkinter.Label(text="POS. SUPERIORE:", fg="black", bg="white")
	l4_pos_sup.grid(column=2, row=15)
	l4a_pos_sup = tkinter.Label(text=pos_sup, fg="black", bg="white")
	l4a_pos_sup.grid(column=3, row=15)

#valutazione variabili
	# if plc_var[4]==False:
		# print("emergenza!!!!!!!!!!!!!!!!!!")
	print("variabili")
	for k in range(len(Plc_vars)):
		print(Plc_vars[k][0]," ",plc_var[k].get_state(),"\n")
	root.update()
	time.sleep(0.15)
#FINE UPDATE
#tl.start(block=False)

#--------------------------------------FINE INPUT/OUTPUT-----------------------------------
print("variabili")
for k in range(len(Plc_vars)):

	print(Plc_vars[k][1],"\n")



btn_quit = tkinter.Button(root, text="Quit", width=8,
	command= root.quit)
btn_quit.bind("<Button-1>", btn_quit_clicked)
btn_quit.grid(column=3, row=0)
# t_update.start()
# t_update = threading.Thread(target = update_plc_vars)
# t_update=threading.Timer(1.0, update_plc_vars,(data, ))
# t_update.start()
# print("update is alive: ",t_update.isAlive())
# tl.start(block=False)
root.geometry('800x500+300+250')
root.bind(btn_quit, lambda e: root.destroy())
#root.protocol("WM_DELETE_WINDOW", btn_quit_clicked())
clear_commands()
root.mainloop()
