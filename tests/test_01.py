#!/usr/bin/env python3

import socket
import struct
import logging
import sys

logging.basicConfig(level=logging.DEBUG)

SERVER_ADDRESS = sys.argv[1]
PLC_STATUS = 12
USER_COMMAND = "CLOSE_SUP"
PLC_USER_COMMAND = 1 << 2


logging.debug('Starting test')
logging.debug('--------------------------------')
logging.debug('Server address:' + SERVER_ADDRESS)
logging.debug('--------------------------------')

logging.debug('Connecting to PLC socket')
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.settimeout(5)
s.connect((SERVER_ADDRESS, 30000))
logging.debug('Sending PLC status')
data = struct.pack('I', PLC_STATUS)
s.sendall(data)
logging.debug('Receiving user command')
data = s.recv(5)
logging.debug('Received data:')
logging.debug(data)
if data != b'\x00':
    logging.error('Different command')
    sys.exit(1)

logging.debug('--------------------------------')

logging.debug('Connecting to user socket')
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.settimeout(5)
s.connect((SERVER_ADDRESS, 23501))
logging.debug('Sending user command')
s.sendall(bytes(USER_COMMAND, encoding='utf8'))
logging.debug('Receiving PLC status')
data = s.recv(1024)
logging.debug('Received data:')
logging.debug(data)
status, = struct.unpack('I', data)
logging.debug(status)
if status != PLC_STATUS:
    logging.error('Different status')
    sys.exit(1)

logging.debug('--------------------------------')

logging.debug('Connecting to PLC socket')
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.settimeout(5)
s.connect((SERVER_ADDRESS, 30000))
logging.debug('Sending PLC status')
data = struct.pack('I', PLC_STATUS)
s.sendall(data)
logging.debug('Receiving user command')
data = s.recv(5)
logging.debug('Received data:')
logging.debug(data)
command, = struct.unpack('I', data)
logging.debug(command)
if command != PLC_USER_COMMAND:
    logging.error('Different command')
    sys.exit(1)

logging.debug('--------------------------------')
logging.debug('All done!')
