#include <SPI.h>
#include <Ethernet.h>

const int PIN_ENCODER_DOME = A0;
const int PIN_ENCODER_AR   = A1;
const int PIN_ENCODER_DEC  = A2;

const int PIN_ROTATION_LEFT   = 3;
const int PIN_ROTATION_RIGHT  = 4;

const int PIN_ALARM_1 = 5;
const int PIN_ALARM_2 = 6;

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
IPAddress ip(192, 168, 40, 124);
IPAddress dns(8, 8, 8, 8);
IPAddress gateway(192, 168, 40, 1);
IPAddress subnet(255, 255, 255, 0);

EthernetServer server(20001);

void setup() {

    pinMode(PIN_ROTATION_LEFT,  OUTPUT);
    pinMode(PIN_ROTATION_RIGHT, OUTPUT);

    pinMode(PIN_ALARM_1, INPUT);
    pinMode(PIN_ALARM_2, INPUT);

    Ethernet.begin(mac, ip, dns, gateway, subnet);

    server.begin();
}

// reading buffer
char in_char;
// output buffer
byte output[3];

// alarms variables (W)
byte alarms_header = 0x57;
byte alarms = 0;
int tmpval;

// encoders variables
int encoders[3];
/*
  Headers for encoders output byte:
  - C: dome
  - A: AR
  - D: DEC
 */
byte encoders_headers[] = {0x43, 0x44, 0x41};


void loop() {

    // Reading alarms
    tmpval = digitalRead(PIN_ALARM_1);
    alarms = alarms | tmpval;

    tmpval = digitalRead(PIN_ALARM_2);
    alarms = alarms | (tmpval << 1);

    // Listening for clients
    EthernetClient client = server.accept();
    if (client) {
        if(client.available() > 0) {
            // leggo
            in_char = client.read();
            // L command
            if (in_char == 0x4C) {
                digitalWrite(PIN_ROTATION_LEFT, HIGH);
            }
            // R command
            if (in_char == 0x52) {
                digitalWrite(PIN_ROTATION_RIGHT, HIGH);
            }
            // S command
            if (in_char == 0x53) {
                digitalWrite(PIN_ROTATION_LEFT,  LOW);
                digitalWrite(PIN_ROTATION_RIGHT, LOW);
            }
            // G command
            if (in_char == 0x47) {
                // Reading encorders
                encoders[0] = analogRead(PIN_ENCODER_DOME);
                encoders[1] = analogRead(PIN_ENCODER_AR);
                encoders[2] = analogRead(PIN_ENCODER_DEC);

                // output encoders
                for (int i = 0; i < 3; i++) {
                    output[0] = encoders_headers[i];
                    output[1] = (encoders[i] & 0xFF00) >> 8;
                    output[2] = (encoders[i] & 0x00FF);
                    client.write(output, 3);
                }
            }

            // output alarms
            output[0] = alarms_header;
            output[1] = alarms;
            output[2] = 0x0;
            client.write(output, 3);
        }

        client.stop();
    }
}
